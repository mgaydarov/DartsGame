#!/usr/bin/perl

use strict;
use warnings;
use Storable qw/store_fd/;
use Game;

my $game = Game->new({
	'name' => '501',
	'rules' => "Each player starts with 501 points. The number of points collected while hitting a board with a dart is subtracted from the given player's points. The winner is the player who scores exactly 0 points that way.

It is a double out game, which means that players must hit a double that makes their score exactly zero to win the game.

In case of a bust the player's score from the previous turn is restored. There is bust if one of the following events arise:
	- The player scores more points in the active turn, than his current score (subtracting would result in a negative score)
	- The player has 1 point after subtracting (you cannot score 1 with double out) 
	- The player has 0 point after subtracting but violates the double-out rule 

Players continue playing until one of them scores 0 points in total. The player who does so, wins the game.
",
	'startPoints' => 501,
	'endPoints' => 0,
	'throws' => 1,
	'players' => undef,
	'gameLogic' => sub {
		my $playerPoints = shift();
		my $pointsEarned = shift();
		if (($playerPoints - $pointsEarned < 0 || $playerPoints - $pointsEarned == 1) || ($playerPoints - $pointsEarned == 0 && $pointsEarned % 2 != 0)) {
		    return $playerPoints;
		}
		return $playerPoints - $pointsEarned;
	}
});

$Storable::Deparse = 1;
open my $FILE, '>', '501.game';
store_fd $game, $FILE;
close $FILE;
