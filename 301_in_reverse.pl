#!/usr/bin/perl

use strict;
use warnings;
use Storable qw/store_fd/;
use Game;

my $game = Game->new({
	'name' => '301 in reverse',
	'rules' => "Each player starts with 0 points. The number of points collected while hitting a board with a dart is added to the given player's points. The winner is the player who scores exactly 301 points that way.

It is a double out game, which means that players must hit a double that makes their score exactly 301 to win the game.

In case of a bust the player's score from the previous turn is restored. There is bust if one of the following events arise:
	- The player scores more points in the active turn, than his current score (adding would result in a score higher than 301)
	- The player has 300 point after addition (you cannot score 1 with double out) 
	- The player has 301 point after addition but violates the double-out rule 

Players continue playing until one of them scores 301 points in total. The player who does so, wins the game.
",
	'startPoints' => 0,
	'endPoints' => 301,
	'throws' => 1,
	'players' => undef,
	'gameLogic' => sub {
		my $playerPoints = shift();
		my $pointsEarned = shift();
		if (($playerPoints + $pointsEarned > 301 || $playerPoints + $pointsEarned == 300) || ($playerPoints + $pointsEarned == 301 && $pointsEarned % 2 != 0)) {
		    return $playerPoints;
		}
		return $playerPoints + $pointsEarned;
	}
});

$Storable::Deparse = 1;
open my $FILE, '>', '301_in_reverse.game';
store_fd $game, $FILE;
close $FILE;
