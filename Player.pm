#!/usr/bin/perl

package Player;

use strict;
use warnings;

sub new {
	my ($class, $args) = @_;

	my $self = {
		name => $args->{'name'},
		gamePoints => $args->{'gamePoints'}
	};

	bless($self, $class);
	return $self;
}

sub getPoints {
	my $self = shift;
	return $self->{'gamePoints'};
}

sub setPoints {
	my $self = shift;
	my $newPoints = shift;
	$self->{'gamePoints'} = $newPoints;
}

return 1;
