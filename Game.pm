#!/usr/bin/perl

package Game;

use strict;
use warnings;
use Storable qw/fd_retrieve/;

sub new {
	my ($class, $args) = @_;

	my $self = {
		name => $args->{'name'},
		rules => $args->{'rules'},
		startPoints => $args->{'startPoints'},
		endPoints => $args->{'endPoints'},
		gameLogic => $args->{'gameLogic'},
		throws => $args->{'throws'},
		players => $args->{'players'},
		isOver => 0
	};

	bless($self, $class);
	return $self;
}

sub calculatePoints {
	my $self = shift;
	my $player = shift;
	my $pointsEarned = shift;

	my $newPoints = &{$self->{'gameLogic'}}($self->{'players'}->{"$player"}->getPoints(), $pointsEarned);
	$self->{'players'}->{"$player"}->setPoints($newPoints);
	return ($newPoints == $self->{'endPoints'});
}

sub getStartPoints {
	my $self = shift;
	return $self->{'startPoints'};
}

sub setPlayers {
	my $self = shift;
	my $players = shift;

	$self->{'players'} = $players;
}

sub loadFromFile {
	my $self;
	my $class = shift;
	my $filename = shift;

	$Storable::Eval = 1;
	open my $GAME, '<', "$filename";
	$self = fd_retrieve($GAME);
	close $GAME;

	bless($self, $class);
	return $self;
}

sub printGameInformation {
	my $self = shift;

	print "You selected to play $self->{'name'}\n";
	print "Rules are: $self->{'rules'}\n";
	print "Provide player names separated by spaces\n";
}

sub play {
	my $self = shift;

	while (1) {
		foreach my $user (keys(%{$self->{'players'}})) {
			my $turn = 0;
			while ($turn < $self->{'throws'}) {
				print "It's $self->{'players'}->{$user}->{'name'} on turn. How much did he/she earned: \n";
				my $userInput = <>;
				$userInput =~ s/[\n\r]+$//g;
				if ($userInput !~ /^[0-9]+$/) {
					print "Invalid points\n";
					next
				}
				if ($self->calculatePoints($user, $userInput)) {
					print "And the winner is: $user\n";
					return $user;
				}
				$turn++;
			}
		}
		print "Current score:\n";
		foreach my $user (keys(%{$self->{'players'}})) {
			print "User $self->{'players'}->{$user}->{'name'} has $self->{'players'}->{$user}->{'gamePoints'}\n";
		}
	}
}

return 1;
