#!/usr/bin/perl

package LeaderBoard;

use strict;
use warnings;
use Storable qw/thaw/;

sub new {
	my ($class, $args) = @_;

	my $self = {};

	bless($self, $class);
	return $self;
}

sub loadFromFile {
	my $self;
	my $class = shift;
	my $filename = shift;

	open my $BOARD, '<', "$filename";
	while (my $line = <$BOARD>) {
		$self .= $line;
	}
	$self = thaw(reverse($self));
	close $BOARD;

	bless($self, $class);
	return $self;
}

sub increasePoints {
	my $self = shift;
	my $player = shift;

	$self->{$player}++;
}

sub addPlayer {
	my $self = shift;
	my $player = shift;

	if (! exists($self->{$player})) {
		$self->{$player} = 0;
	}
}

sub printLeaderBoard {
	my $self = shift;
	print "Leader Board /user, points/\n";
	foreach my $key (sort { $self->{$b} <=> $self->{$a} } (keys(%{$self}))) {
		print "$key: $self->{$key}\n";
	}
}

return 1;
