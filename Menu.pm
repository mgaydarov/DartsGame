#!/usr/bin/perl

package Menu;

use strict;
use warnings;

sub new {
	my $class = shift;

	my $self = {};

	bless($self, $class);
	return $self;
}

sub printMenus {
	my $self = shift;
	print "1. Show Leader Board\n";
	print "2. Start new game\n";
	print "3. Create new game\n";
	print "4. Exit\n";
}

sub invalidOption {
	my $self = shift;

	print "Invalid option selected. Please choose again.\n";
}

sub invalidGame {
	my $self = shift;

	print "Invalid game selected. Please choose again.\n";
}

sub listAllGames {
	opendir(my $DIR, '.');
	while (my $entry = readdir($DIR)) {
		next if ($entry =~ /^\.{1,2}$/);
		next if ($entry !~ /\.game$/);
		$entry =~ s/\.game$//g;
		print "$entry\n";
	}
	closedir($DIR);
}

return 1;
