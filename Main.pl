#!/usr/bin/perl

use strict;
use warnings;
use Scalar::Util;
use Storable qw/freeze/;
use Game;
use Player;
use LeaderBoard;
use Menu;

my $leaderBoardFile = 'leaderboard.db';

my $menu = Menu->new();
my $leaderBoard;
if (-r "$leaderBoardFile") {
	$leaderBoard = LeaderBoard->loadFromFile("leaderboard.db");
} else {
	$leaderBoard = LeaderBoard->new();
}
while (1) {
	$menu->printMenus();
	my $userInput = <>;
	$userInput =~ s/[\n\r]+$//g;
	if ($userInput !~ /^[1-4]$/) {
		$menu->invalidOption();
		next;
	}
	if ($userInput == 1) {
		$leaderBoard->printLeaderBoard();
		next;
	}
	if ($userInput == 2) {
		while (1) {
			$menu->listAllGames();
			$userInput = <>;
			$userInput =~ s/[\n\r]+$//g;
			if (! -r "$userInput.game") {
				$menu->invalidGame();
				next;
			}
			my $game = Game->loadFromFile("$userInput.game");
			$game->printGameInformation();
			$userInput = <>;
			$userInput =~ s/[\n\r]+$//g;
			my @players = split(/\s+/, $userInput);
			my $playerObjects = {};
			foreach my $player (@players) {
				my $object = Player->new({'name' => $player, 'gamePoints' => $game->getStartPoints()});
				$leaderBoard->addPlayer($player);
				$playerObjects->{$player} = $object;
			}
			$game->setPlayers($playerObjects);
			my $winner = $game->play();
			$leaderBoard->increasePoints("$winner");
			last;
		}
		next;
	}
	if ($userInput == 3) {
		# Create New Game
		next;
	}
	if ($userInput == 4) {
		last;
	}
}
open my $LEADERBOARD, '>', "$leaderBoardFile" or die('Unable to store leader board. Some information is lost.');
print $LEADERBOARD reverse(freeze($leaderBoard));
close $LEADERBOARD;
